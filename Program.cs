﻿using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            Console.WriteLine("Ex 20 - Part A");

            float num1 = 0.0f;
            float num2 = 0.0f;

            Console.WriteLine("Please enter a number");
            num1 = float.Parse(Console.ReadLine());
            Console.WriteLine("Please enter a second number");
            num2 = float.Parse(Console.ReadLine());

            Console.WriteLine($"Your first number was {num1}, your second number was {num2}\n\n");
            if (num1 > num2)
            {
                Console.WriteLine($"num1({num1}) is greater than num2({num2})");
            }
            else if (num1 < num2)
            {
                Console.WriteLine($"num1({num1}) is less than num2({num2})");
            }
            else if (num1 == num2)
            {
                Console.WriteLine($"num1({num1}) is equal to num2({num2})");
            }

            // Part B
            Console.WriteLine("\n\n");
            Console.WriteLine("Ex 20 - Part B");
            string aPassword = "";

            Console.WriteLine("Please enter your password:");
            Console.WriteLine("A valid password will be 8 characters or more");
            aPassword = Console.ReadLine();
            if (aPassword.Length < 8)
            {
                Console.WriteLine("Invalid password: A password must contain at least 8 characters,");
                Console.WriteLine($"your password contained {aPassword.Length} characters");
                aPassword = "";
            }
            else{
                Console.WriteLine("Password set successfully =]");
            }

            // Part C
            Console.WriteLine("\n\n");
            Console.WriteLine("Ex 20 - Part C");
            string oldPass = "password";
            string newPass = "";

            Console.WriteLine("Hint: Default password = password\n");

            Console.WriteLine("Please enter a new password");
            Console.WriteLine("A valid password will be 8 characters or more\n");

            newPass = Console.ReadLine();

            if (newPass.Length < 8)
            {
                Console.WriteLine("Invalid password: A password must contain at least 8 characters,");
                Console.WriteLine($"your password contained {aPassword.Length} characters");
            }
            else if (newPass == oldPass)
            {
                Console.WriteLine("Invalid password: Your new password must be different to your old password");
            }
            else{
                Console.WriteLine("Success");
                oldPass = oldPass.Replace(oldPass, newPass);
            }
            
            Console.WriteLine($"Current password = {oldPass}");
            

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
